### to research and deploy
https://openliberty.io/guides/#persistence

https://www.sqlshack.com/sql-database-on-kubernetes-considerations-and-best-practices/

# for mysql pv
mkdir -p /mnt/data

# get a db dump
https://www.mysqltutorial.org/how-to-load-sample-database-into-mysql-database-server.aspx
copy to the server where pod is running in /mnt/data/ so it can be accessed inside the pod

# load some db into the pod
kubectl exec -it pod/mysql-68579b78bb-qm24b -- sh
mysql -uroot -ppassword
source /var/lib/mysql/tests/mysqlsampledatabase.sql


## To Do: MySQL Master with many slaves cluster
https://kublr.com/blog/setting-up-mysql-replication-clusters-in-kubernetes-2/

# to add in liberty config and dir structure
use `curl` on each node as described in repo/readme.md

# to check the Default Datasource on liberty
https://tdb1.kubes/ibm/api/validation/dataSource/DefaultDataSource
`userName="blogAdmin" userPassword="blogAdminPassword`

# to check the replication user was created (only on first time creation, AKA if /mnt/data doesn't exist)
kubectl exec -it pod/mysql-master-0 -- sh
mysql -uroot -p$MYSQL_ROOT_PASSWORD

use mysql;
SELECT
    user,
    host,
    password,
    index_priv
FROM
    user;

# create some dummy tables in the master's tests db
```
kubectl exec -it mysql-master-0 -- sh
mysql -uroot -p$MYSQL_ROOT_PASSWORD
use tests;
create table user(id int(10), name char(20));
insert into user values(100, 'user1');
select * from user;
```

# check replication works on slave(s)
```
kubectl exec -it mysql-slave-0 -- sh
mysql -uroot -p$MYSQL_ROOT_PASSWORD
use tests;
select * from user;
```

# in case of errors see more info
```
show slave status\G;
```
