apiVersion: apps/v1
kind: StatefulSet
metadata:
  name: mysql-slave
spec:
  replicas: 1
  selector:
    matchLabels:
      app: mysql
      dbnode: slave
  serviceName: mysql-slave
  template:
    metadata:
      labels:
        app: mysql
        dbnode: slave
    spec:
      initContainers:
      - name: init-mysql-slave
        image: mysql:5.6
        command:
        - bash
        - "-c"
        - |
          set -ex
          # Generate mysql server-id from pod ordinal index
          [[ `hostname` =~ -([0-9]+)$ ]] || exit 1
          ordinal=${BASH_REMATCH[1]}
          # Add an offset to avoid reserved server-id=0 value
          echo '[mysqld]' >> /etc/mysql/mysql.conf.d/mysqld.cnf
          echo server-id=$((100 + $ordinal)) >> /etc/mysql/mysql.conf.d/mysqld.cnf
          echo 'log-bin' >> /etc/mysql/mysql.conf.d/mysqld.cnf
          echo 'datadir		= /var/lib/mysql' >> /etc/mysql/mysql.conf.d/mysqld.cnf
          echo 'symbolic-links=0' >> /etc/mysql/mysql.conf.d/mysqld.cnf
        volumeMounts:
        - name: mysql-slave-etc-storage
          mountPath: /etc/mysql/mysql.conf.d/
      containers:
      - image: mysql:5.6
        name: mysql-slave
        env:
        - name: MYSQL_ROOT_PASSWORD
          value: password
        - name: MYSQL_REPLICATION_USER
          valueFrom:
            secretKeyRef:
              name: mysql-secret
              key: replicationusername
        - name: MYSQL_REPLICATION_PASSWORD
          valueFrom:
            secretKeyRef:
              name: mysql-secret
              key: replicationpassword
        ports:
        - containerPort: 3306
          name: mysql
        volumeMounts:
        - name: mysql-slave-persistent-storage
          mountPath: /var/lib/mysql
        - name: mysql-slave-etc-storage
          mountPath: /etc/mysql/mysql.conf.d/

        - name: mysql-slave-init
          mountPath: /docker-entrypoint-initdb.d/01-slave-replication-setup.sh
          subPath: 01-slave-replication-setup.sh
      volumes:
      - name: mysql-slave-etc-storage
        persistentVolumeClaim:
          claimName: mysql-slave-etc-pvc
      - name: mysql-slave-persistent-storage
        persistentVolumeClaim:
          claimName: mysql-slave-pvc
      - name: mysql-slave-init
        configMap:
          name: mysql-slave-init
        