### install cert-manager
```
helm repo add jetstack https://charts.jetstack.io
helm repo update
helm install cert-manager jetstack/cert-manager --namespace cert-manager --create-namespace --version v1.3.1 --set installCRDs=true
kubectl get all -n cert-manager
kubectl get pods -n cert-manager
```
### self sign a test cert in a test namespace
```
kubectl apply -f secure-env-test.yaml
```
### check cert
```
kubectl describe -n secure-env-test certificate selfsigned-damewas.com-cert
```

### point to was
```
kubectl apply -f secure-ingress.yaml
```

configure a reverse proxy or just add in `/etc/hosts` an entry to your server/s for fqdn `damewas.com` e.g. :
```
192.168.110.151 tdb1.kubes tdb1 damewas.com
```

now if you access a web browser you should see your self-signed certificate being used in https://damewas.com/

### for cleanup just delete the namespace secure-env-test or using kubectl delete -f for each file

### more info and official doc

[Cert-manager installation docs](https://cert-manager.io/docs/installation/kubernetes/)
