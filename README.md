### some test not for tdb lol

install argocd and its cli
```
kubectl create namespace argocd
kubectl apply -n argocd -f https://raw.githubusercontent.com/argoproj/argo-cd/stable/manifests/install.yaml
VERSION=$(curl --silent "https://api.github.com/repos/argoproj/argo-cd/releases/latest" | grep '"tag_name"' | sed -E 's/.*"([^"]+)".*/\1/')
curl -sSL -o /usr/local/bin/argocd https://github.com/argoproj/argo-cd/releases/download/$VERSION/argocd-linux-amd64
chmod +x /usr/local/bin/argocd
```

wait until all pods are running, change password and then make the service accesible externally
```
### wait a bit or check the pods before doing any modifications
kubectl get all -n argocd
kubectl patch svc argocd-server -n argocd -p '{"spec": {"type": "NodePort"}}'
ARGO_SVC_NODEPORT=$(kubectl get svc argocd-server -n argocd -o=jsonpath='{.spec.ports[?(@.port==80)].nodePort}')
kubectl -n argocd get secret argocd-initial-admin-secret -o jsonpath="{.data.password}" | base64 -d >>pass
argocd login $(hostname -f):$ARGO_SVC_NODEPORT
argocd account update-password
echo "go have fun at $(hostname -f):$ARGO_SVC_NODEPORT"
```

app conf
```
# lazy solution is hostPath on each server (can easily be changed in deployment for NFS or something else)
# to add in liberty config and dir structure

use `curl` on each node as described in repo/readme.md
```

```
# use the cli or dashboard to create a new app using the repo https://gitlab.com/fede_e_b/not-tdb.git
# for a basic sample of argocd manifest see: liberty-argocd.yaml 
```