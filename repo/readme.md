### artifact usage for each node of the cluster
to get the local repo populated in `/opt`
```
curl https://gitlab.com/fede_e_b/not-tdb/-/archive/main/not-tdb-main.tar?path=repo | tar -xf - not-tdb-main-repo/repo --strip-components 1 -C /opt --overwrite
```